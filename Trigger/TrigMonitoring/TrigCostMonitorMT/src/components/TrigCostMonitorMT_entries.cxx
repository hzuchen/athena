/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "src/TrigCostMTSvc.h"
#include "TrigCostMonitorMT/TrigCostMTAuditor.h"
#include "TrigCostMonitorMT/EnhancedBiasWeightCompAlg.h"

DECLARE_COMPONENT( TrigCostMTSvc )
DECLARE_COMPONENT( TrigCostMTAuditor )
DECLARE_COMPONENT( EnhancedBiasWeightCompAlg )
